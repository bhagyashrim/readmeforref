# Azure Migration(Azure Site Recovery) #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary : Directory containes sript to migrate your on-premise servers to azure cloud using Azure Site Recovery.

### How do I get set up? ###

* Steps to setup: 
You can run script from windows machine.	
connect to  windows machine.	
login to azure account using azlogin and azconnect.	
run the script through powershell admin.	
Provide required paramaters sucas credentials,virtual machine list.	

* Script details:
pass the input csv file which has server list to migrate to azure cloud	
pass the  input csv file with user list to enable replication	
connect to az account	
specify subscriptiion ID	
Verify that the Configuration server is successfully registered to the vault	
Create replication policy if not	
Track Job status to check for completion	
Create Failback Policy if not	
Track Job status to check for completion	
Get the protection container corresponding to the Configuration Serve	
Get the replication policies to map by name.	
Associate the replication policies to the protection container corresponding to the Configuration Server.	
Verify that mandatory properties are defined for each object	
Enable replication for  virtual machine

